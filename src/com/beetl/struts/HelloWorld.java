package com.beetl.struts;
import com.opensymphony.xwork2.ActionSupport;

public class HelloWorld extends ActionSupport {


    public String execute() throws Exception {
        this.message = "hello world ajax";
//        throw new RuntimeException("error");
        return SUCCESS;
    }

    /**
     * Provide default valuie for Message property.
     */
    public static final String MESSAGE = "HelloWorld.message";

    /**
     * Field for Message property.
     */
    private String message;

    /**
     * Return Message property.
     *
     * @return Message property
     */
    public String getMessage() {
        return message;
    }

}
